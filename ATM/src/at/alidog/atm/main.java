package at.alidog.atm;

import java.util.Scanner;
import java.util.concurrent.TimeUnit;

public class main {

	public static void main(String[] args) throws InterruptedException {

		Scanner sc = new Scanner(System.in);
		int input;
		int balance = 0;
		boolean finsihed = false;
		boolean wait = false;
		String message = new String();
		atm myAtm = new atm(100, 50);

		while (!finsihed) {
			if(wait) {
			TimeUnit.SECONDS.sleep(2);
			}
			System.out.println("\nPlease choose a option");
			System.out.println("\n1: Account Balance");
			System.out.println("\n2: Deposit");
			System.out.println("\n3: Withdraw");
			System.out.println("\n4: Close");
			input = sc.nextInt();
			wait = true;
			switch (input) {
			case 1:
				System.out.println("\nBalance: " + myAtm.getBalance());
				break;
				
			case 2:
				System.out.println("\nPlease enter the Amount you wish to deposit");
				input = sc.nextInt();
				message = myAtm.deposit(input);
				System.out.println(message);
				break;
				
			case 3:
				System.out.println("\nPlease enter the Amount you wish to withdraw");
				input = sc.nextInt();
				message = myAtm.withdraw(input);
				System.out.println(message);
				break;
				
			case 4:
				System.out.println("\nClosed successfully");
				System.exit(0);
				finsihed = true;
				break;
				
			default:
				System.out.println("\nWrong Input. Try again!");

			}

		}
	}

}
