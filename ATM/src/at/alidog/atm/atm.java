package at.alidog.atm;

public class atm {

	public atm(int limit, int balance) {
		super();
		this.limit = limit;
		this.balance = balance;
	}

	private int limit;
	private int balance;

	public int getLimit() {
		return limit;
	}

	public int getBalance() {
		return balance;
	}

	public String deposit(int amount) {

		balance += amount;

		String message = "\nDeposit successful! New balance: " + balance;

		return message;

	}

	public String withdraw(int amount) {

		String message;

		if (balance < amount) {
			message = "\nInsufficient Funds!";

		} else {
			balance -= amount;

			message = "\nWithdraw successful! New balance: " + balance;
		}

		return message;

	}
}
